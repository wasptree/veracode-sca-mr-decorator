# Veracode SCA MR Decorator for Gitlab
<!-- ABOUT THE PROJECT -->
## About This Project

veracode-sca-mr-decorator is python script to automate the process of adding a Veracode SCA Analysis summary output onto a Gitlab merge request (MR) discussion board from the CI/CD pipeline.

This is to help developers and reviewers see security analysis results directly on the merge request discussion board.

This is not an official Veracode project.


<!-- GETTING STARTED -->
## Getting Started

Docker:
A pre-built amd64 versions of the veracode-sca-decorator image containing the dependencies and python script is available on docker hub [hub.docker.com/veracode-sca-decorator](https://hub.docker.com/repository/docker/wasptree/veracode-sca-decorator)

Alternatively build a local image from this repository:

  ```sh
    $ git clone https://gitlab.com/wasptree/veracode-sca-decorator
    $ cd veracode-sca-decorator
    $ docker build -t veracode-sca-decorator .
  ```

Python Script:
Download and call the python script. The python-gitlab module is required.
All required dependencies are included in the veracode-libs folder.

<!-- SETUP -->
## Setup

For the script to be able authenticate to Gitlab and comment on the current merge request, a Gitlab private token is required.

1. Generate a valid Gitlab token for your project
   ```
   Settings -> Access Tokens -> Create Project access token
   ```
2. Set this token as an environment variable for use in the CI pipeline. In this example we use the name GITLAB_PRIVATE_TOKEN
   ```
   Settings -> CI/CD -> Variables
   ```

<!-- USAGE EXAMPLES -->
## gitlab-ci Examples

The following 2 gitlab-ci jobs show using the Veracode pipeline scan agent to perform a SCA analysis and save a JSON output to the file sca_results.json 

This summary report is then commented on the merge request. The CI_PROJECT_ID and CI_MERGE_REQUEST_IID are default environment variables available within the Gitlab CI when a merge request pipeline executes. The GITLAB_PRIVATE_TOKEN must be set in the CI/CD settings for authentication to Gitlab to make the comment on the MR.

Checkout the .gitlab-ci.yml file and Jenkinsfile in this project.
```
veracode_sca:
    image: maven:3.6.0-jdk-8
    stage: SCA
    script:
      - curl -sSL https://download.sourceclear.com/ci.sh | sh -s . scan --scan-collectors mvn --json sca-results.json
    artifacts:
      when: always
      paths:
        - sca-results.json
      expire_in: 1 day
    allow_failure: true
    

comment_sca_results:
    image: wasptree/veracode-sca-decorator
    stage: report
    script:
     - python3 veracode-sca-decorator -f sca-results.json -p ${CI_PROJECT_ID} -m {CI_MERGE_REQUEST_IID} -t ${GITLAB_PRIVATE_TOKEN}
    allow_failure: true

```

## Jenkinsfile / Shell Script Example

In this example we are downloading the python script as a release from this repository.

This example pipeline is triggered by a Merge Request push notification to Jenkins. The Gitlab Jenkins plugin makes the following required environment variables available in the pipeline:

gitlabMergeRequestTargetProjectId  
gitlabMergeRequestIid


```
stage('Security - Veracode SCA') {
            steps {
                    withCredentials([string(credentialsId: 'SRCCLR_API_TOKEN', variable: 'SRCCLR_API_TOKEN')]) {
                    sh 'curl -sSL https://download.sourceclear.com/ci.sh | sh -s . scan --scan-collectors mvn --json sca-results.json'
                }

            }
        }
        stage('Giltab MR Decoration') {
            steps {
                    withCredentials([string(credentialsId: 'gitlab_decorator', variable: 'gitlab_token')]) {
                    sh 'curl -LO https://gitlab.com/wasptree/veracode-sca-decorator/-/archive/latest/veracode-sca-decorator-latest.tar.gz'
                    sh 'tar -xvzf veracode-sca-decorator-latest.tar.gz'
                    sh 'python3 ./veracode-sca-decorator-latest/veracode-sca-decorator.py -f sca-results.json -p ${gitlabMergeRequestTargetProjectId} -m ${gitlabMergeRequestIid} -t ${gitlab_token}'
                }
            }
        }
```

## Screenshot Example

![](/images/2023-10-10-13-02-42.png)

<!-- To Do -->
## To Do

1. Nightly build for Docker image
2. Add policy evaluation check and messaging
