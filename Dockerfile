FROM python:3.8-slim-bullseye

RUN mkdir /opt/veracode

WORKDIR /opt/veracode

COPY 'veracode-sca-decorator.py' /opt/veracode/veracode-sca-decorator.py
COPY 'requirements.txt' /opt/veracode/requirements.txt

RUN pip install -r requirements.txt

CMD ["python", "veracode-sca-decorator.py"]
