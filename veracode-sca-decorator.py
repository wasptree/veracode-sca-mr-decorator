import json
import argparse
import sys
import os
sys.path.append(os.path.join(sys.path[0],'veracode-libs'))
import gitlab

def parse_args():
    global args

    parser = argparse.ArgumentParser(description='Veracode SCA report to \
            Comment on Gitlab merge request')
    parser.add_argument("-f", "--filename", required=True, help="The SCA results to comment")
    parser.add_argument("-u", "--url", default="https://gitlab.com/", help="The URL for private Gitlab server")
    parser.add_argument("-t", "--token", help="Access token for Gitlab server")
    parser.add_argument("-p", "--pid", required=True, help="Gitlab CI project ID")
    parser.add_argument("-m", "--mid", required=True, help="Gitlab CI Merge request ID")
    
    args = parser.parse_args()
    
    filename = args.filename
    if not os.path.exists(filename):
        print(" [!] Report file is not accessible. Write SCA JSON output to a file")
    return filename

def get_api_token():
    try:
        token = os.environ['GITLAB_PRIVATE_TOKEN']
    except:
        print(" [!] GITLAB_PRIVATE_TOKEN environment variable is not set")
        exit(1)
    return token

def get_gitlab_ids():
    try:
        project_id = os.environ['gitlabMergeRequestTargetProjectId']
    except:
        print(" [!] env variable gitlabMergeRequestTargetProjectId is not set. Is this a Gitlab merge request?")
        exit(1)
    try:
        mr_iid = os.environ['gitlabMergeRequestIid']
    except:
        print(" [!] env variable gitlabMergeRequestIid is not set. Is this a merge request?")
        exit(1)
    return project_id, mr_iid 

# Function to load and parse JSON
def load_and_parse_json(file_path):
    try:
        # Open the JSON file for reading
        with open(file_path, 'r') as file:
            # Load the JSON data
            data = json.load(file)
            return data
    except FileNotFoundError:
        print(f"File not found: {file_path}")
        return None
    except json.JSONDecodeError as e:
        print(f"Error parsing JSON: {str(e)}")
        return None

def count_license_risk(data):
    high_count = 0 
    medium_count = 0
    low_count = 0


    for record in data["records"][0]["libraries"]:
        versions = record.get("versions")
        for version in versions:
            licenses = version.get("licenses")
            for license in licenses:
                if license["risk"] == "HIGH":
                    high_count += 1
                if license["risk"]  == "MEDIUM":
                    medium_count += 1
                if license["risk"]  == "LOW":
                    low_count += 1
    
    return high_count, medium_count, low_count
        

def count_vulns(data):
    
    # Define the severity score thresholds
    critical_threshold = 8.1
    high_threshold = 6.1
    medium_threshold = 4.1
    
    critical_count = 0
    high_count = 0
    medium_count = 0
    low_count = 0

    criticals = []
    highs = []
    mediums = []
    lows = []

    for record in data["records"][0]["vulnerabilities"]:
        cvss3_score = record.get("cvss3Score", 0)  # Default to 0 if score is missing
        cve = record.get("cve")
        title = record.get("title")

        if cvss3_score >= critical_threshold:
            critical_count += 1
            criticals.append({"cve": cve, "title" : title, "severity": cvss3_score})
        elif cvss3_score >= high_threshold:
            high_count += 1
            highs.append({"cve": cve, "title" : title, "severity": cvss3_score})
        elif cvss3_score >= medium_threshold:
            medium_count += 1
            mediums.append({"cve": cve, "title" : title, "severity": cvss3_score})
        else:
            low_count += 1
            lows.append({"cve": cve, "title" : title, "severity": cvss3_score})

    return critical_count, high_count, medium_count, low_count

def get_vuln_list(data):

    CVE_list = []

    for record in data["records"][0]["vulnerabilities"]:
        try:
            CVE = "CVE-" + record["cve"]
        except TypeError as e:
            CVE = "No CVE"
        severity = record["cvss3Score"]
        link = record["_links"]["html"]
        title = record["title"]
        CVE_list.append({"CVE": CVE, "Severity": severity, "link": link, "Title": title})

    # Deduplicate the list based on CVE number
    unique_CVE_list = []
    seen_CVEs = set()

    for cve_info in CVE_list:
        if cve_info["CVE"] not in seen_CVEs:
            seen_CVEs.add(cve_info["CVE"])
            unique_CVE_list.append(cve_info)

    CVE_list_sorted = sorted(unique_CVE_list, key=lambda x: x["Severity"], reverse=True)

    markdown_table = "\n### CVE&emsp;&emsp;Severity&emsp;&emsp;Title  \n\n"

    for cve_info in CVE_list_sorted:
        markdown_table += f"[{cve_info['CVE']}]({cve_info['link']})&emsp;&emsp;{cve_info['Severity']}&emsp;&emsp;{cve_info['Title']}  \n"

    return markdown_table


def get_license_list(data):
    license_list = []

    for record in data["records"][0]["libraries"]:
        versions = record.get("versions")
        for version in versions:
            licenses = version.get("licenses")
            for license in licenses:
                license_list.append({"license" : license["license"], "risk" : license["risk"]})
    
    unique_license_list = []
    seen_licenses = set()

    for license_info in license_list:
        if license_info["license"] not in seen_licenses:
            seen_licenses.add(license_info["license"])
            unique_license_list.append(license_info)

    risk_mapping = {"HIGH": 3, "MEDIUM": 2, "LOW": 1}
    license_list_sorted = sorted(unique_license_list, key=lambda x: risk_mapping.get(x["risk"], 0), reverse=True)

    markdown_table = "\n### License&emsp;&emsp;Veracode Risk Rating  \n\n"

    for license_info in license_list_sorted:
        markdown_table += f"{license_info['license']}&emsp;&emsp;{license_info['risk']}  \n"

    return markdown_table
  

def format_output(data, vuln_list, license_list):

    critical_count, high_count, medium_count, low_count = count_vulns(data)
    vuln_high_count, vuln_medium_count, vuln_low_count = count_license_risk(data)

    scan_url = data['records'][0]['metadata']['report']

    image_url = "https://gitlab.com/wasptree/veracode-glmc/-/raw/master/images/veracode-black-hires.svg"
    image_html = f'<img src="{image_url}" width="300" height="100px">'

    scan_results = f'''
{image_html} &emsp;
### Software Composition Analysis
Scan link : {scan_url} 

### Vulnerabilities
| $`\\textcolor{{Maroon}}{{\\textbf{{Critical}}}}`$ | $`\\textcolor{{FireBrick}}{{\\textbf{{High}}}}`$ | $`\\textcolor{{Orange}}{{\\textbf{{Medium}}}}`$ | $`\\textcolor{{Gold}}{{\\textbf{{Low}}}}`$ | 
| :---:      | :---:      | :---:      | :---:      |
| **{critical_count}**   | **{high_count}**   | **{medium_count}**   | **{low_count}**   |  

<details><summary>Details</summary>  

{vuln_list}

</details>  

### License Risk
| $`\\textcolor{{FireBrick}}{{\\textbf{{High}}}}`$ | $`\\textcolor{{Orange}}{{\\textbf{{Medium}}}}`$ | $`\\textcolor{{Gold}}{{\\textbf{{Low}}}}`$ | 
| :---:      | :---:      | :---:      |
| **{vuln_high_count}**   | **{vuln_medium_count}**   | **{vuln_low_count}**   |  

<details><summary>Details</summary>  

{license_list}

</details>  
'''

    return scan_results

def gitlab_client(gitlab_url, access_token):
    gl = gitlab.Gitlab(gitlab_url, private_token=access_token, ssl_verify=False)

    try:
        gl.auth()
        print(f"Successfully connected to {gitlab_url}")
    except gitlab.GitlabAuthenticationError:
        print(f"Failed to authenticate with {gitlab_url}")
    except Exception as e:
        print(f"An error occurred: {str(e)}")

    return gl

def main():

    parse_args()

    data = load_and_parse_json(args.filename)

    vuln_list = get_vuln_list(data)

    license_list = get_license_list(data)
    
    scan_results = format_output(data, vuln_list, license_list)

    #project_id, mr_iid = get_gitlab_ids()

    project_id = args.pid

    mr_iid = args.mid

    gl = gitlab_client(args.url, args.token)
    project = gl.projects.get(project_id)
    mr = project.mergerequests.get(mr_iid)

    ## Save the note to the merge request
    mr_note = mr.notes.create({'body': scan_results})

if __name__ == "__main__":
    main()